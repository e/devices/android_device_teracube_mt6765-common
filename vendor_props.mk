ifeq (eng,$(TARGET_BUILD_VARIANT))
VENDOR_LOG_LEVEL=I
else
VENDOR_LOG_LEVEL=S
endif

# AEE
PRODUCT_PROPERTY_OVERRIDES += \
    ro.vendor.have_aee_feature=1

# Audio
PRODUCT_PROPERTY_OVERRIDES += \
    ro.vendor.mtk_audio_alac_support=1 \
    ro.vendor.mtk_audio_ape_support=1 \
    ro.vendor.mtk_audio_tuning_tool_ver=V2.2 \
    ro.vendor.mtk_besloudness_support=1

# Bluetooth
PRODUCT_PROPERTY_OVERRIDES += \
    ro.vendor.bt.platform=connac1x \
    ro.vendor.connsys.dedicated.log.port=bt,wifi,gps,mcu \
    ro.vendor.mtk.bt_sap_enable=true

# Camera
PRODUCT_PROPERTY_OVERRIDES += \
    camera.disable_zsl_mode=1 \
    persist.vendor.camera3.pipeline.bufnum.base.imgo=4 \
    persist.vendor.camera3.pipeline.bufnum.base.lcso=4 \
    persist.vendor.camera3.pipeline.bufnum.base.rrzo=4 \
    persist.vendor.camera3.pipeline.bufnum.base.rsso=5 \
    persist.vendor.camera3.pipeline.bufnum.min.high_ram.fdyuv=5 \
    persist.vendor.camera3.pipeline.bufnum.min.high_ram.imgo=7 \
    persist.vendor.camera3.pipeline.bufnum.min.high_ram.lcso=7 \
    persist.vendor.camera3.pipeline.bufnum.min.high_ram.rrzo=7 \
    persist.vendor.camera3.pipeline.bufnum.min.high_ram.rsso=7 \
    persist.vendor.camera3.pipeline.bufnum.min.low_ram.fdyuv=5 \
    persist.vendor.camera3.pipeline.bufnum.min.low_ram.imgo=6 \
    persist.vendor.camera3.pipeline.bufnum.min.low_ram.lcso=6 \
    persist.vendor.camera3.pipeline.bufnum.min.low_ram.rrzo=6 \
    persist.vendor.camera3.pipeline.bufnum.min.low_ram.rsso=6 \
    ro.vendor.camera.isp.support.colorspace=0 \
    ro.vendor.camera3.zsl.default=130 \
    ro.vendor.mtk_cam_security_support=1 \
    ro.vendor.mtk_camera_app_version=4 \
    ro.vendor.mtk_external_sim_only_slots=0 \
    ro.vendor.mtk_zsdhdr_support=1 \
    vendor.camera.aux.packagelist=org.codeaurora.snapcam,com.android.camera,foundation.e.camera \
    vendor.camera.mdp.cz.enable=1 \
    vendor.camera.mdp.dre.enable=0

# DRAM
PRODUCT_PROPERTY_OVERRIDES += \
    ro.vendor.mtk_config_max_dram_size=0x800000000

# DRM
PRODUCT_PROPERTY_OVERRIDES += \
    drm.service.enabled=true \
    ro.vendor.mtk_widevine_drm_l3_support=1 \
    ro.vendor.mtk_mvpu_security_support=0

# Dex
PRODUCT_PROPERTY_OVERRIDES += \
    dalvik.vm.dex2oat64.enabled=true

# FM
PRODUCT_PROPERTY_OVERRIDES += \
    persist.vendor.connsys.fm_chipid=mt6631

# FRP
PRODUCT_PROPERTY_OVERRIDES += \
    ro.frp.pst=/dev/block/platform/bootdevice/by-name/frp

# Graphics
PRODUCT_PROPERTY_OVERRIDES += \
    debug.renderengine.backend=skiaglthreaded \
    debug.sf.use_phase_offsets_as_durations=1 \
    debug.sf.disable_backpressure=1 \
    debug.sf.early.app.duration=20000000 \
    debug.sf.early.sf.duration=27600000 \
    debug.sf.earlyGl.app.duration=20000000 \
    debug.sf.earlyGl.sf.duration=27600000 \
    debug.sf.hwc.min.duration=17000000 \
    debug.sf.late.app.duration=20000000 \
    debug.sf.late.sf.duration=27600000 \
    renderthread.skia.reduceopstasksplitting \
    ro.hardware.egl=meow \
    ro.opengles.version=196610 \
    ro.vendor.pq.mtk_backlight_smooth_support=1 \
    ro.vendor.pq.mtk_ultra_dimming_support=0 \
    ro.vendor.pq.mtk_blulight_def_support=1

# LMK
PRODUCT_PROPERTY_OVERRIDES += \
    ro.lmk.downgrade_pressure=60 \
    ro.lmk.kill_timeout_ms=100 \
    ro.lmk.psi_complete_stall_ms=150 \
    ro.lmk.swap_free_low_percentage=20 \
    ro.lmk.swap_util_max=90 \
    ro.lmk.thrashing_limit=30 \
    ro.lmk.thrashing_limit_decay=50

# OEM
PRODUCT_PROPERTY_OVERRIDES += \
    ro.oem_unlock_supported=1

# Media
PRODUCT_PROPERTY_OVERRIDES += \
    debug.stagefright.c2inputsurface=-1 \
    media.c2.dmabuf.padding=3072 \
    media.stagefright.thumbnail.prefer_hw_codecs=true \
    ro.vendor.mtk_pq_color_mode=1 \
    ro.vendor.mtk_pq_support=2 \
    ro.vendor.pq.mtk_pq_video_whitelist_support=0 \
    ro.vendor.pq.mtk_video_transition=0 \
    ro.vendor.pq.mtk_scltm_support=0 \
    ro.vendor.mml.mtk_mml_support=0 \
    ro.vendor.pq.mtk_ai_scence_pq_support=0 \
    ro.vendor.pq.mtk_hdr10_plus_recording_support=0 \
    ro.vendor.pq.mtk_ai_sdr_to_hdr_support=0 \
    ro.vendor.pq.mtk_ultra_resolution_support=0 \
    ro.vendor.pq.mtk_dc_support=0 \
    ro.vendor.pq.mtk_ds_support=0 \
    ro.vendor.pq.mtk_hfg_support=0 \
    ro.vendor.pq.mtk_caltm_support=1 \
    ro.vendor.pq.mtk_clearzoom_support=0 \
    ro.vendor.pq.mtk_mdp_ccorr_support=1 \
    ro.vendor.pq.mtk_pq_interface_support=0 \
    ro.vendor.pq.mtk_disp_c3d_support=0 \
    ro.vendor.pq.mtk_disp_tdshp_support=0 \
    ro.vendor.pq.mtk_disp_color_support=0 \
    ro.vendor.pq.mtk_disp_ccorr_support=0 \
    ro.vendor.pq.mtk_disp_gamma_support=0 \
    ro.vendor.pq.mtk_aal_support=1 \
    ro.vendor.pq.mtk_dre30_support=0 \
    ro.vendor.pq.mtk_disp_game_pq_support=0 \
    ro.vendor.pref_scale_enable_cfg=1 \
    vendor.mtk.vdec.waitkeyframeforplay=9

# NFC
PRODUCT_PROPERTY_OVERRIDES += \
    ro.vendor.mtk_nfc_addon_support=1

# GPS
PRODUCT_PROPERTY_OVERRIDES += \
    ro.vendor.gps.chrdev=gps_drv \
    ro.vendor.mtk_agps_app=1 \
    ro.vendor.mtk_gps_support=1 \
    ro.vendor.mtk_log_hide_gps=1

# Radio
PRODUCT_PROPERTY_OVERRIDES += \
    persist.radio.multisim.config=dsds \
    persist.vendor.pco5.radio.ctrl=0 \
    persist.vendor.radio.fd.counter=150 \
    persist.vendor.radio.fd.off.counter=50 \
    persist.vendor.radio.fd.off.r8.counter=50 \
    persist.vendor.radio.fd.r8.counter=150 \
    persist.vendor.mims_support=2 \
    persist.vendor.factory.GB2312=no \
    persist.vendor.md_c2k_cap_dep_check=0 \
    persist.vendor.mtk_sim_switch_policy=2 \
    persist.vendor.radio.mtk_dsbp_support=1 \
    persist.vendor.radio.mtk_ps2_rat=L/W/G \
    persist.vendor.radio.mtk_ps3_rat=G \
    persist.vendor.radio.smart.data.switch=1 \
    ro.telephony.default_network=9,9,9,9 \
    ro.telephony.iwlan_operation_mode=AP-assisted \
    ro.telephony.sim.count=2 \
    ro.vendor.mtk_fd_support=1 \
    ro.vendor.mtk_single_bin_modem_support=1 \
    ro.vendor.mtk_wapi_support=1 \
    ro.vendor.mtk.bt_sap_enable=true \
    ro.vendor.mtk_embms_support=1 \
    ro.vendor.mtk_sim_card_onoff=3 \
    ro.vendor.num_md_protocol=2 \
    ro.vendor.mtk_protocol1_rat_config=Lf/Lt/W/G \
    ro.vendor.mtk_c2k_lte_mode=0 \
    ro.vendor.mtk_lte_support=1 \
    ro.vendor.mtk_world_phone_policy=0 \
    ro.vendor.sim_me_lock_mode=0 \
    ro.vendor.mtk_external_sim_only_slots=0 \
    ro.vendor.mtk_md1_support=14 \
    ro.vendor.mtk_eccci_c2k=1 \
    ro.vendor.mtk_ril_mode=c6m_1rild \
    ro.vendor.mtk_md_world_mode_support=1 \
    ro.vendor.mtk_data_config=1 \
    ro.vendor.md_prop_ver=1 \
    ro.vendor.mtk_ril_mode=c6m_1rild \
    ro.vendor.mtk_rild_read_imsi=1 \
    ro.vendor.mtk_telephony_add_on_policy=0 \
    ro.vendor.radio.max.multisim=dsds \
    telephony.active_modems.max_count=2

# IMS
PRODUCT_PROPERTY_OVERRIDES += \
    persist.vendor.ims_support=1 \
    persist.vendor.mtk_dynamic_ims_switch=1 \
    persist.vendor.mtk_wfc_support=1 \
    persist.vendor.volte_support=1 \
    persist.vendor.mtk.volte.enable=1 \
    persist.vendor.vilte_support=1 \
    persist.vendor.viwifi_support=1 \
    ro.vendor.md_auto_setup_ims=1 \
    persist.vendor.mtk_ct_volte_support=3 \
    persist.dbg.volte_avail_ovr=1 \
    persist.dbg.vt_avail_ovr=1 \
    persist.dbg.wfc_avail_ovr=1

# Storage
PRODUCT_PROPERTY_OVERRIDES += \
    persist.sys.fuse.passthrough.enable=true \
    ro.vendor.mtk_emmc_support=1 \
    ro.crypto.volume.filenames_mode=aes-256-cts

# Sensors
PRODUCT_PROPERTY_OVERRIDES += \
    ro.jichuang_fpsensor=1 \
    ro.vendor.mtk.sensor.support=yes \
    ro.vendor.init.sensor.rc=init.sensor_1_0.rc

# Neural Networks
PRODUCT_PROPERTY_OVERRIDES += \
    debug.mtk_tflite.target_nnapi=29 \
    ro.vendor.mtk_nn.option=A,B,C,D,E,F,G,Z \
    ro.vendor.mtk_nn_quant_preferred=1

# WFD
PRODUCT_PROPERTY_OVERRIDES += \
    ro.vendor.mtk_wfd_support=1 \
    ro.vendor.wfd.dummy.enable=0 \
    ro.vendor.wfd.iframesize.level=0

# Mediatek
PRODUCT_PROPERTY_OVERRIDES += \
    ro.vendor.mediatek.platform=MT6765 \
    ro.vendor.mtk_prefer_64bit_proc=0 \
    ro.vendor.mediatek.version.branch=alps-mp-s0.mp1 \
    ro.vendor.mediatek.version.release=alps-mp-s0.mp1.rc-V9.16.3_lentek.s0mp1rc.k61v1.64.bsp_P6

# WiFi
PRODUCT_PROPERTY_OVERRIDES += \
    persist.vendor.connsys.chipid=-1 \
    persist.vendor.connsys.patch.version=-1 \
    persist.vendor.connsys.dynamic.dump=0 \
    ro.vendor.connsys.dedicated.log=1 \
    ro.vendor.wlan.gen=gen4m \
    ro.vendor.wlan.chrdev=wmt_chrdev_wifi \
    ro.vendor.wifi.sap.concurrent.iface=ap1 \
    ro.vendor.wifi.sap.interface=ap0 \
    ro.vendor.mtk_wappush_support=1 \
    ro.vendor.ap_info_monitor=0 \
    ro.vendor.wlan.standalone.log=y \
    vendor.connsys.driver.ready=no \
    wifi.interface=wlan0

# Logging
PRODUCT_PROPERTY_OVERRIDES += \
    persist.log.tag.AT=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.AdnRecord=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.AdnRecordCache=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.AdnRecordLoader=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.AirplaneHandler=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.CapaSwitch=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.CarrierExpressServiceImpl=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.CarrierExpressServiceImplExt=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.CountryDetector=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.DSSelector=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.DSSelectorOP01=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.DSSelectorOP02=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.DSSelectorOP09=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.DSSelectorOP18=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.DSSelectorOm=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.DSSelectorUtil=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.DataDispatcher=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.DcFcMgr=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.GsmCallTkrHlpr=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.GsmCdmaConn=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.GsmCdmaPhone=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.IccCardProxy=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.IccPhoneBookIM=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.IccProvider=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.ImsApp=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.ImsCall=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.ImsCallProfile=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.ImsCallSession=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.ImsEcbm=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.ImsEcbmProxy=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.ImsManager=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.ImsPhone=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.ImsPhoneBase=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.ImsPhoneCall=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.ImsService=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.ImsVTProvider=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.IsimFileHandler=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.IsimRecords=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MTKSST=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkAdnRecord=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkCsimFH=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkDc=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkDcc=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkDct=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkEmbmsAdaptor=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkFactory=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkGsmCdmaConn=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkIccCardProxy=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkIccPHBIM=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkIccProvider=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkImsManager=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkImsService=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkIsimFH=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkPhoneNotifr=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkPhoneNumberUtils=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkPhoneSwitcher=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkRecordLoader=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkRetryManager=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkRuimFH=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkSIMFH=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkSIMRecords=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkSmsCbHeader=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkSmsManager=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkSmsMessage=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkSpnOverride=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkSubCtrl=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkUiccCard=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkUiccCardApp=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkUiccCtrl=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MtkUsimFH=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.NetAgent_IO=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.NetLnkEventHdlr=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.NetworkPolicy=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.OperatorUtils=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.PQ_DS=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.Phone=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.PhoneConfigurationSettings=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.PhoneFactory=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.PowerHalAddressUitls=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.PowerHalMgrImpl=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.PowerHalMgrServiceImpl=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.PowerHalWifiMonitor=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.ProxyController=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RFX=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RTC_DAC=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxAction=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxChannelMgr=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxCloneMgr=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxContFactory=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxController=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxDebugInfo=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxDisThread=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxFragEnc=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxHandlerMgr=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxIdToMsgId=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxIdToStr=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxMainThread=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxMclDisThread=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxMclMessenger=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxMclStatusMgr=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxMessage=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxObject=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxOpUtils=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxRoot=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxStatusMgr=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxTimer=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcCapa=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcCdmaSimRequest=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcCdmaSimUrc=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcDcPdnManager=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcDcReqHandler=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcDcUtility=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcEccNumberUrcHandler=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcEmbmsReq=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcEmbmsUrc=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcGsmSimRequest=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcGsmSimUrc=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcImsCtlReqHdl=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcImsCtlUrcHdl=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcNwHdlr=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcNwRTReqHdlr=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcNwReqHdlr=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcOemHandler=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcPhbReq=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcPhbUrc=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcWp=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RtcCapa=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RtcDC=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RtcEccNumberController=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RtcEmbmsAt=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RtcEmbmsUtil=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RtcIms=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RtcImsConference=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RtcImsConfigController=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RtcImsDialog=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RtcModeCont=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RtcNwCtrl=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RtcPhb=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RtcWp=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.SIMRecords=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.SimSwitchOP01=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.SimSwitchOP02=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.SimSwitchOP18=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.SlotQueueEntry=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.SpnOverride=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.Telecom=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.UiccCard=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.UiccController=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.UxUtility=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.VT=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.VsimAdaptor=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WORLDMODE=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WfoApp=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaCcciDataHeaderEncoder=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaCcciReader=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaCcciSender=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaControlMsgHandler=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaDriver=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaDriverAccept=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaDriverAdapter=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaDriverDeReg=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaDriverMessage=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaDriverRegFilter=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaDriverULIpPkt=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaDriverUtilis=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaDriverVersion=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaParsing=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaRingBuffer=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaRuleContainer=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaRuleRegister=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaShmAccessController=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaShmReadMsgHandler=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaShmSynchronizer=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaShmWriteMsgHandler=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.libPowerHal=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.mtkpower@impl=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.mtkpower_client=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.wpfa_iptable_android=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.C2K_RIL-DATA=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.ExternalSimMgr=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.IMSRILRequest=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.IMS_RILA=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.ImsBaseCommands=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcCommSimOpReq=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcCommSimReq=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcCommSimUrc=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcDcCommon=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RtcCommSimCtrl=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.MwiRIL=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RIL-Fusion=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RIL-Netlink=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RIL-Parcel=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RIL-SocListen=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RIL-Socket=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RIL=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RILC-OP=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RILC=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RILD=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RILMD2-SS=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RILMUXD=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RIL_UIM_SOCKET=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RadioManager=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxRilAdapter=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RfxRilUtils=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RilClient=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RilOemClient=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RilOpProxy=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcOpRadioReq=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcRadioReq=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RtcRadioCont=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.WpfaFilterRuleReqHandler=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.NetAgentService=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.NetworkStats=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RmcRatSwHdlr=$(VENDOR_LOG_LEVEL) \
    persist.log.tag.RtcRatSwCtrl=$(VENDOR_LOG_LEVEL)
